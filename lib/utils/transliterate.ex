defmodule Zhugar.Transliterate do
  @moduledoc """
  Этот модуль переводит с русского на транслит
  """
  @ru_eng [
    {:а, "a"},
    {:б, "b"},
    {:в, "v"},
    {:г, "g"},
    {:д, "d"},
    {:е, [
      {0, "ye"},
      {:any, "e"}
    ]},
    {:ё, "e"},
    {:ж, "zh"},
    {:з, "z"},
    {:и, "i"},
    {:й, "y"},
    {:к, "k"},
    {:л, "l"},
    {:м, "m"},
    {:н, "n"},
    {:о, "o"},
    {:п, "p"},
    {:р, "r"},
    {:с, "s"},
    {:т, "t"},
    {:у, "u"},
    {:ф, "f"},
    {:х, "kh"},
    {:ц, "ts"},
    {:ч, "ch"},
    {:ш, "sh"},
    {:щ, "shch"},
    {:ъ, ""},
    {:ы, "y"},
    {:ь, ""},
    {:э, "e"},
    {:ю, "yu"},
    {:я, "ya"}
  ]

  def trlit_word(word, "ru_to_eng") do
    case word do
      nil ->
        ""
      "" ->
        ""
      word ->
        word
        |> String.trim
        |> String.split("")
        |> clean_nils_and_empty
        |> Enum.with_index
        |> Enum.map(fn {letter, index} -> ru_to_eng(letter, index) end)
        |> clean_nils_and_empty
        |> Enum.reduce("", fn letter, acc -> acc <> letter end)
        |> String.capitalize
    end
  end

  @doc """
  Функция сопоставляющая русские буквы с английскими
  """
  def ru_to_eng(letter, pos) do
    letter
    |> String.downcase
    |> trlit_letter(@ru_eng)
    |> interpretate_result(pos)
  end

  defp trlit_letter(letter, rule), do: rule |> List.keyfind(String.to_atom(letter), 0)
  defp interpretate_result({_atom, result}, _pos) when is_binary(result), do: result
  defp interpretate_result(nil, _pos), do: nil
  defp interpretate_result({_atom, result}, pos) when is_list(result) do
    case pos do
      num when is_integer(num) ->
        result |> List.keyfind(num, 0)
      _ ->
        result |> List.keyfind(:any, 0)
    end
  end
  defp clean_nils_and_empty(list) when is_list(list), do: list |> Enum.filter(&(&1))
end
