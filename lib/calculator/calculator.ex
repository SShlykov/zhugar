defmodule Zhugar.Calculator do
  @moduledoc """
  Функции основного калькулятора.
  Калькулятор предназначен для реализации собственного DSL для рассчета фронтов и выполнения прочих простейших операций.
  Основная функция, которая будет использоваться -  evaluate

  ## Например
      # для рассчета фронтов для МТСО заданной машины и даты может быть использована сделающая функция (переменные типа ид машины и параметров авторизации передаются ниже) -
      evaluate(
        ##### ЗАДАЧА 1 - ПОЛУЧЕНИЕ ДАННЫХ #####

        lobj      = base_select(db_auth, machine_id, dt)

        ##### ЗАДАЧА 2 - РАСЧЕТ ФРОНТОВ МТСО #####

        fronts  =
          lobj
          |> parse_dt("dt")
          |> put_map([{"mode", 3}, {"machine_id", {{machine_id}}}])
          |> sort("gps_datetime", "asc_nulls_last")
          |> pack_slice("dt_diff(curr.dt, prev.dt) > 43_200 || curr.d2 > prev.d2")
          |> reduce_pack_to_front(~s|
            ~m{id, imei, mode, machine_id}a = last(packs)
            d2      = max_by(packs, "d2")
            d2_koef = pi() * 0.95 / 15
            speed   = avg_map(packs, "speed")
            gpsses  = get_and_sort(packs, "gps_datetime")
            beg_dt  = first(gpsses)
            end_dt  = last(gpsses)

            coords  = fetch_geo(packs, "gps_lon", "gps_lat")
            {beg_lat, beg_lon}   = first(coords)
            {end_lat, end_lon}   = last(coords)

            dt      = dt_now()
            odometr = round(d2_koef * d2)
            geo     = geomaker(coords)

            ~m{id, imei, dt, beg_dt, end_dt, mode, odometr, speed, beg_lat, beg_lon, end_lat, end_lon, geo, machine_id}a
          |)

        ##### ЗАДАЧА 3 - УДАЛЕНИЕ НЕЗНАЧИТЕЛЬНЫХ ФРОНТОВ #####

        fronts      = reject_unimportant(fronts, "dt_diff(end_dt, beg_dt) < 20")

        ##### ЗАДАЧА 4 - ДОБАВЛЕНИЕ ДАННЫХ О ПИКЕТАХ #####

        fronts    = fetch_pickets(fronts, db_test, 1)

        ##### ЗАДАЧА 5 - ЗАПИСЬ ДАННЫХ #####
        insert_data(fronts, dt, machine_id)
        ,%{
          machine_id: machine_id,
          dt: dt,
          db_auth: %{hostname: {{hostname}}, username: {{user}}, password: {{password}}, database: {{database}}, port: {{port}}},
          db_test: %{hostname: {{hostname}}, username: {{user}}, password: {{password}}, database: {{database}}, port: {{port}}}
        }
      )
  """
  alias Zhugar.Calculator.Helpers
  alias Zhugar.{SqlWorker, HttpWorker}
  import ShorterMaps

  @type db_auth_params :: %{
    hostname: binary(),
    username: binary(),
    password: binary(),
    database: binary(),
    port: integer()
  }

  @type db_params :: list({atom(), any()})
                    | list()
                    | map()

  @type resp_or_error :: {:error, binary()}
                        | map()
                        | list(map())

  @type rules :: {binary(), binary(), binary(), any()}

  @doc """
  Запускает функцию обработки формулы, принимает два параметра - формулу и параметры в виде мапы или списка ключ - значение
  При этом в формуле могут использоваться функции которые описаны в данном модуле


  ## Например

      iex> Zhugar.Calculator.evaluate("a + b", %{a: 2, b: 4})
      6

      iex> Zhugar.Calculator.evaluate("a + b + c", %{a: 2, b: 4})
      ** (CompileError) nofile:2: undefined function c/0

  ###
  """
  @spec evaluate(binary(), [{atom, any}] | map) :: any
  def evaluate(str, params \\ [])
  def evaluate(str, params) when is_list(params) do
    """
    import ShorterMaps
    #{str}
    """
    |> Code.string_to_quoted!()
    |> Helpers.locals_calls_only
    |> Helpers.in_module(__MODULE__)
    |> Code.eval_quoted(params)
    |> elem(0)
  end
  def evaluate(str, params) when is_map(params), do: evaluate(str, Map.to_list(params))

  @doc """
  Делает стандартный SQL запрос, возвращает данные в виде мапы или ошибку
  ## Например

      iex> db_auth = %{hostname: "localhost", username: "postgres", password: "postgres", database: "postgres", port: 5432}
      iex> Zhugar.Calculator.sql_request(db_auth, "select 1 as test")
      %{test: 1}

  """
  @spec sql_request(db_auth_params(), binary(), db_params()) :: resp_or_error()
  def sql_request(pms, query, params \\ []), do: SqlWorker.run_query(pms, query, params)

  @doc """
  Делает стандартный HTTP запрос, возвращает данные в виде мапы или ошибку
  ## Например

      iex> Zhugar.Calculator.http_request("https://jsonplaceholder.typicode.com/todos/1", "get")
      %{completed: false, id: 1, title: "delectus aut autem", userId: 1}

      iex> Zhugar.Calculator.http_request("https://jsonplaceholder.typicode.com/posts", "post", ~s({"id": 101, "title": "foo", "body": "bar", "userId": 1}))
      %{body: "bar", id: 101, title: "foo", userId: 1}

      iex> Zhugar.Calculator.http_request("https://jsonplaceholder.typicode.com/posts", "post", %{id: 101, title: 'foo', body: 'bar', userId: 1})
      %{id: 101, title: 'foo', userId: 1, body: 'bar'}

  """
  @spec http_request(binary(), binary(), binary() | nil) :: resp_or_error()
  def http_request(addr, method, body \\ nil), do: HttpWorker.run_http(%{addr: addr, method: method}, body)

  @doc """
  Функция базового селекта - запрос первичных данных практически похож
  """
  @spec base_select(db_auth_params(), integer(), binary() | nil, binary()) :: resp_or_error()
  def base_select(db, machine_id, nil, type), do: sql_request(db, "select * from askr_locations_#{type} where machine = #{machine_id}")
  def base_select(db, machine_id, dt, type), do: sql_request(db, "select * from askr_locations_#{type} where machine = #{machine_id} and gps_datetime > #{Timex.format!(dt, "%Y-%m-%d %H:%M:%S", :strftime)}")

  @doc """
  Получить текущее время на сервере

  ## Например
      iex> %DateTime{} = dt = Zhugar.Calculator.dt_now
      iex> is_nil(dt)
      false
  """
  @spec dt_now :: DateTime.t()
  def dt_now, do: Timex.now()
  @doc """
  Получить число пи

  ## Например
      iex> :math.pi() == Zhugar.Calculator.pi
      true
  """
  @spec pi :: float
  def pi, do: :math.pi()

  @doc """
  Получить последний элемент массива

  ## Например
      iex> list = [1,2,4,1,3]
      iex> Zhugar.Calculator.last(list)
      3

      iex> list = [%{a: 1},%{a: 2},%{a: 4},%{a: 6},%{a: 1}]
      iex> Zhugar.Calculator.last(list)
      %{a: 1}
  """
  @spec last([any]) :: any
  def last(list), do: List.last(list)

  @doc """
  Получить первый элемент массива

  ## Например
      iex> list = [1,2,4,1,3]
      iex> Zhugar.Calculator.first(list)
      1

      iex> list = [%{a: 1},%{a: 2},%{a: 4},%{a: 6},%{a: 3}]
      iex> Zhugar.Calculator.first(list)
      %{a: 1}
  """
  @spec first([any]) :: any
  def first(list), do: List.first(list)

  @doc """
  Удаляет не значимые пакеты. Если параметр не существует или условие - объект отбрасывается

  ## Например
      iex> list = [%{a: 1},%{a: 2},%{a: 4},%{a: 6},%{a: 3}]
      iex> Zhugar.Calculator.reject_unimportant(list, "a+1 <= 2")
      [%{a: 2}, %{a: 4}, %{a: 6}, %{a: 3}]

      iex> list = [%{a: 1},%{a: 2},%{a: 4},%{a: 6},%{a: 3}]
      iex> Zhugar.Calculator.reject_unimportant(list, "b <= 2")
      []
  """
  @spec reject_unimportant(map(), binary()) :: [any] | nil
  def reject_unimportant(packs, condition), do: Enum.reject(packs, & try_evaluate(condition, &1))

  @doc """
  Собирается список фронтов из списка списков пакетов изначально разделенных на будущие фронты по заданному условию.
  условно [[пак1, пак2, пак3, ...], [пак1, пак2, пак3, ...], ...] -> [фронт1, фронт2, ...]
  Рекурсивно применяет функции записанные в формуле

  ## Например

      iex> list = [[%{a: 1, b: 5},%{a: 2, b: 5},%{a: 4, b: 5},%{a: 6, b: 5},%{a: 3, b: 5}], [%{a: 1, b: 2},%{a: 2, b: 2},%{a: 4, b: 2},%{a: 6, b: 2},%{a: 3, b: 2}]]
      iex> Zhugar.Calculator.reduce_pack_to_front(list, ~s|
      ...>    ~m{b}a = last(packs)
      ...>    a = avg_map(packs, "a")
      ...>    front_param = b + a
      ...>    ~m{front_param}a
      ...> |)
      [%{front_param: 8.2}, %{front_param: 5.2}]

      iex> list = [[%{a: 1},%{a: 2},%{a: 4},%{a: 6},%{a: 3}], [%{a: 1},%{a: 2},%{a: 4},%{a: 6},%{a: 3}]]
      iex> Zhugar.Calculator.reduce_pack_to_front(list, ~s|
      ...>    ~m{b}a = last(packs)
      ...>    a = avg_map(packs, "a")
      ...>    front_param = b + a
      ...>    ~m{front_param}a
      ...> |)
      []
  """
  @spec reduce_pack_to_front(list(map()), any) :: [any]
  def reduce_pack_to_front(list, fml), do: list |> Enum.map(& reduce_to_front(&1, fml)) |> reject_errors

  @doc """
  Находит ближайшие пикеты и добавляет значения к фронту

  """
  @spec fetch_pickets(list(map()), db_auth_params(), integer()) :: [any]
  def fetch_pickets(fronts, db, dev), do: Enum.map(fronts, &fetch_closest(&1, db, dev))

  @doc """
  Вставляет данные по указанным параметрам
  """
  @spec insert_data(list(map()), db_auth_params(), integer()) :: :ok
  def insert_data(fronts, db, machine_id) do
    # TODO make dynamic multsert
    {fronts, db, machine_id}
    :ok
  end

  @doc """
  Добавляет ключ значение в один из типов - мапа или список из мап. Значение может быть рекурсивно вычислено
  (имеет доступ к элементам мапы, в которую подкладывается значение)

  ## Например

      iex> Zhugar.Calculator.put_map([%{a: 2}], "b", "4 * 2 - 1")
      [%{a: 2, b: 7}]

      iex> Zhugar.Calculator.put_map([%{a: 2}], "b", "a * 2 - 1")
      [%{a: 2, b: 3}]
  """
  @spec put_map(maybe_improper_list | map, any, any, any) :: [any] | map
  def put_map(any, key, expression, params \\ [])
  def put_map(list, key, expression, params) when is_list(list), do: Enum.map(list, & put_map(&1, key, expression, params))
  def put_map(map, key, expression, params) when is_map(map), do: Map.put(map, String.to_atom(key), try_evaluate("#{expression}", params ++ Enum.to_list(map)))

  @doc """
  Делает тоже самое, что Zhugar.Calculator.put_map/3 , но принимает список из параметров и формул. Выполняет функции рекурсивно.
  Может использовать ранее указанные параметры (но не позднее)
  Выкинет первую ошибку с названием параметра

  ## Например

      iex> Zhugar.Calculator.put_map([%{a: 2}], [{"b", "4 * 2 - 1"}, {"c", "(a + b)/2"}])
      [%{a: 2, b: 7, c: 4.5}]

      iex> Zhugar.Calculator.put_map([%{a: 2}], [{"b", "4 * 2 - 1"}, {"c", "(a + b)/2"}, {"d", "pi() * 2"}])
      [%{a: 2, b: 7, c: 4.5, d: 6.283185307179586}]

      iex> Zhugar.Calculator.put_map([%{a: 2}], [{"d", "4 * 2 - 1"}, {"c", "(a + b)/2"}, {"b", "pi() * 2"}])
      ** (RuntimeError) cant be calculated param: `c`
  """
  @spec put_map(maybe_improper_list | map, list | map) :: any
  def put_map(any, list_expressions)
  def put_map(list, list_expressions) when is_map(list_expressions), do: put_map(list, Enum.to_list(list_expressions))
  def put_map(list, list_expressions) when is_list(list) and is_list(list_expressions), do: Enum.map(list, & put_map(&1, list_expressions))
  def put_map(map, list_expressions) when is_map(map) do
    list_expressions
    |> Enum.reduce(map, fn {key, expression}, acc ->
      val = try_evaluate("#{expression}", Enum.to_list(acc))
      if is_binary(val) && String.match?(val, ~r/error/), do: raise("cant be calculated param: `#{key}`")
      Map.put(acc, String.to_atom(key), val)
    end)
  end

  @doc """
  Распаковывает пакеты по будущим фронтам (т.е. формирует список из списков согласно условию), условие подразумевает выделение места разделения пакета (например, когда одометр падает или когда дистанция между фронтами больше определенной величины)

  Внутри имеется доступ к следующим объектам -
  prev - предыдущий
  next - следующий
  last - последний
  first - первый
  curr - текущий
  idx - индекс текущего элемента в общем массиве пакетов
  pack_list - массив пакетов

  ## Например

      iex> Zhugar.Calculator.pack_slice([%{a: 1},%{a: 2},%{a: 4},%{a: 6},%{a: 3},%{a: 4},%{a: 2},%{a: 1}], "curr.a < prev.a")
      [[%{a: 1}, %{a: 2}, %{a: 4}, %{a: 6}], [%{a: 3}, %{a: 4}], [%{a: 2}], [%{a: 1}]]
  """
  @spec pack_slice(list(map()), binary()) :: [any]
  def pack_slice(pack_list, condition) do
    pack_list
    |> Enum.with_index
    |> Enum.map(fn {curr, idx} ->
      prev  = Enum.at(pack_list, idx - 1)
      next  = Enum.at(pack_list, idx + 1)
      last  = List.last(pack_list)
      first = List.first(pack_list)

      if prev && try_evaluate(condition, ~M{prev, next, curr, last, first}),
        do: [:f, curr],
        else: [:t, curr]
    end)
    |> Enum.reduce([[]], fn [pred, x], [head | tail] ->
      if pred == :t, do: [head ++ [x] | tail], else: [[x] | [head | tail]]
    end)
    |> Enum.reject(& &1 == [])
    |> Enum.reverse()
  end

  @doc """
  Перерабатывает объект согласно правилам (например, при переработке списка фронтов)
  Второй параметр является списком из правил, сами правила строятся из 4х параметров -
  {название поля, алиас (как оно будет записано), формула вычисления или обработки, дефолтное значение}

  ## Например

      iex> Zhugar.Calculator.ret([%{a: 2}, %{a: 3}], [{"b", "b", "3*4*a", 0}])
      [%{b: 24}, %{b: 36}]


  """
  @spec ret(maybe_improper_list, list(rules()), any) :: [any]
  def ret(lst, ltpl, params \\ %{}) when is_list(lst) and is_list(ltpl) do
    lst
    |> Enum.map(fn obj ->
      Enum.reduce(ltpl, %{}, & reduce_ret_field(&1, &2, params, obj))
    end)
  end

  @doc """
  Возвращает первое успешное значение из выполненных условий -

  ## Например

      iex> map = %{a: 3, b: 3}
      iex> Zhugar.Calculator.conddo([
      ...>    ["a > b", "a more than b"],
      ...>    ["a == b", "a * b"],
      ...>    [true, "a less then b"]
      ...> ], map)
      9
  """
  @spec conddo(list(list()), list()) :: any
  def conddo(opt, params \\ []) do
    opt
    |> Enum.reduce(false, fn x, acc ->
      cond do
        !!acc                                     -> acc
        List.first(x) |> try_execute_it(params)   -> List.last(x) |> try_execute_it(params)
        true                                      -> false
      end
    end)
  end

  @doc """
  Получить значение и отсортировать его (полезно, например, для дат или неструктурированных списков)

  ## Например

      iex> Zhugar.Calculator.get_and_sort([%{a: 1},%{a: 2},%{a: 4},%{a: 6},%{a: 3},%{a: 4},%{a: 2},%{a: 1}], "a")
      [1, 1, 2, 2, 3, 4, 4, 6]

  """
  @spec get_and_sort(maybe_improper_list, binary) :: [any]
  def get_and_sort(list, key) when is_list(list), do: list |> get_by_keys(key) |> Enum.sort

  @doc """
  Получить максимальное значение в списке

  ## Например

      iex> Zhugar.Calculator.max_by([%{a: 1},%{a: 2},%{a: 4},%{a: 6},%{a: 3},%{a: 4},%{a: 2},%{a: 1}], "a")
      6

  """
  @spec max_by(list(map()), binary) :: any
  def max_by(list, key) when is_list(list), do: list |> get_by_keys(key) |> Enum.max |> Kernel.||(0)

  @doc """
  Получить среднее значение в списке из мап

  ## Например

      iex> Zhugar.Calculator.avg_map([%{a: 1},%{a: 2},%{a: 4},%{a: 6},%{a: 3},%{a: 4},%{a: 2},%{a: 1}], "a")
      2.875

  """
  @spec avg_map(list(map()), binary) :: float
  def avg_map(list, key) when is_list(list) do
    list |> get_by_keys(key) |> Enum.sum() |> Kernel./(length(list))
  end

  @doc """
  Получить среднее значение в списке

  ## Например

      iex> Zhugar.Calculator.avg([1,2,3,4,5,6,7,8,9,2,43,54,2])
      11.23076923076923

  """
  @spec avg(list(integer() | float())) :: float
  def avg(list) when is_list(list), do: Enum.sum(list) |> Kernel./(length(list))

  @doc """
  Посчитать сумму (явно выкинет ошибку при вычислении)

  ## Например

      iex> Zhugar.Calculator.sum(23, 123)
      146

  """
  @spec sum(integer, integer) :: integer
  def sum(a, b) when is_integer(a) and is_integer(b), do: a + b
  def sum(_a, _b), do: throw("you can summ only ints")

  @doc """
  Переименовывает значение согласно заданному алиасу (либо создаст необходимое поле) -
  тип изменения говорит о том, перезапишется ли значение (:hard) или проигнорируется в случае, если значение уже существует

  ## Например

      iex> Zhugar.Calculator.field_alias([%{a: 2}], "a", "b", :soft)
      [%{a: 2, b: 2}]

      iex> Zhugar.Calculator.field_alias([%{a: 4, b: 2}], "a", "b", :soft)
      [%{a: 4, b: 2}]

      iex> Zhugar.Calculator.field_alias([%{a: 4, b: 2}], "a", "b", :hard)
      [%{a: 4, b: 4}]

      iex> Zhugar.Calculator.field_alias([%{a: 2}], "a", "a", :soft)
      [%{a: 2}]

  """
  @spec field_alias([any], binary, binary, :hard | :soft) :: [any]
  def field_alias(lst, key, alis, type \\ :soft) do
    case type do
      :soft -> Enum.map(lst, & Map.merge(%{String.to_atom(alis) => &1[String.to_atom(key)]}, &1))
      :hard -> Enum.map(lst, & Map.put(&1, String.to_atom(alis), &1[String.to_atom(key)]))
    end
  end

  @doc """
  Получить значения по заданным ключам

  ## Например

      iex> Zhugar.Calculator.get_by_keys([%{a: 2}], "a")
      [2]
  """
  @spec get_by_keys(maybe_improper_list, binary) :: [any]
  def get_by_keys(lom, key) when is_list(lom) and is_binary(key), do: lom |> Enum.map(& Map.get(&1, String.to_atom(key)))

  @doc """
  Получить картежи координатных пар ({lon, lat}) из списка пакетов по именам этих параметров в пакете

  ## Например

      iex> Zhugar.Calculator.fetch_geo([%{a: 2, b: 5}], "a", "b")
      [{2, 5}]
  """
  @spec fetch_geo(maybe_improper_list, binary, binary) :: [{any, any}]
  def fetch_geo(lom, lnname, ltname) when is_list(lom) do
    lats   = lom |> get_by_keys(ltname)
    lons   = lom |> get_by_keys(lnname)
    Enum.zip(lons, lats)
  end

  @doc """
  Создать гео объект, готовый к загрузке в базе - создает либо точку либо кривую
  """
  @spec geomaker([any]) :: nil | map
  def geomaker(coords) when is_list(coords) do
    if length(coords) > 0, do: try_linestring(coords), else: nil
  end

  @doc """
  Парсит время по ключу внутри списка или объекта из строк в формат даты и дата-времени
  """
  @spec parse_dt(maybe_improper_list | map, any) :: [any] | map
  def parse_dt(list, key) when is_list(list), do: Enum.map(list, & parse_dt(&1, String.to_atom(key)))
  def parse_dt(map, key) when is_map(map) and is_atom(key), do: Map.put(map, key, parse_dt(map[key]))
  def parse_dt(map, key) when is_map(map), do: Map.put(map, String.to_atom(key), parse_dt(map[key]))

  @doc """
  Перерабатывает дату и дату-время из строк в нужный формат
  может принять 3 формата времени - yy:mm:dd, yyyy:mm:dd, yyyy:mm:dd hh:mm:ss
  """
  @spec parse_dt(binary) :: any
  def parse_dt(""), do: nil
  def parse_dt(dt) when is_binary(dt) do
    dt =
      case String.length(dt) do
        8    -> "20#{dt} 00:00:00"
        10   -> "#{dt} 00:00:00"
        _dt  -> dt
      end

    try do
      Timex.parse!(dt, "%Y-%m-%d %H:%M:%S", :strftime)
    rescue
      _e -> throw("Invalid date-time format")
    end
  end
  def parse_dt(dt), do: dt

  @doc """
  Находит разницу во времени в заданной величине (третий параметр определяет величину определения)

  ## Например

      iex> Zhugar.Calculator.dt_diff("2021-01-28 01:19:41", "2021-01-28 01:19:47")
      6
  """
  @spec dt_diff(binary(), binary(), atom()) :: integer() | float()
  def dt_diff(dt1, dt2, time_vol \\ :second)
  def dt_diff(dt1, dt2, time_vol) when is_binary(dt1) and is_binary(dt2), do: Timex.diff(parse_dt(dt1), parse_dt(dt2), time_vol) |> abs()
  def dt_diff(dt1, dt2, time_vol), do: Timex.diff(dt1, dt2, time_vol) |> abs()

  @doc """
  Находит расстояние между двумя координатами типа {lon, lat}
  """
  @spec geo_diff(tuple, tuple) :: nil | float
  def geo_diff(geo1, geo2) when is_tuple(geo1) and is_tuple(geo2) do
    try do
      Geocalc.distance_between(geo1 |> Tuple.to_list, geo2 |> Tuple.to_list)
    rescue
      _e -> nil
    end
  end

  @doc """
  Находит расстояние между двумя координатами типа {lon, lat} по заданным ключам
  """
  @spec geo_diff(map, map, any, any) :: nil | float
  def geo_diff(map1, map2, lat_name, lon_name) when is_map(map1) and is_map(map2) do
    ln1 = Map.get(map1, lon_name)
    ln2 = Map.get(map2, lon_name)
    lt1 = Map.get(map1, lat_name)
    lt2 = Map.get(map2, lat_name)

    geo_diff({ln1, lt1}, {ln2, lt2})
  end

  @doc """
  Фильтрует значения (мапы) по ключу и условию в сроке
  """
  @spec filter(list(any), binary, binary) :: [any]
  def filter(lst, key, condition) when is_list(lst) and is_binary(key) and is_binary(condition), do: Enum.filter(lst, & try_execute_it("#{&1[String.to_atom(key)]} #{condition}"))


  @doc """
  Сортирует значения (мапы) по ключу
  """
  @spec sort(any, binary, binary) :: [any]
  def sort(lst, key, order \\ "asc") do
    key = String.to_atom(key)
    order = String.to_atom(order)
    case order do
      :asc                -> Enum.sort_by(lst, & &1[key], order)
      :desc               -> Enum.sort_by(lst, & &1[key], order)
      :asc_nulls_last     -> Enum.sort(lst, & asc_nulls_last(&1, &2, key))
      :desc_nulls_last    -> Enum.sort(lst, & desc_nulls_last(&1, &2, key))
      :asc_nulls_first    -> Enum.sort(lst, & asc_nulls_first(&1, &2, key))
      :desc_nulls_first   -> Enum.sort(lst, & desc_nulls_first(&1, &2, key))
      _ -> throw("Cant be sorted. Option is invalid")
    end
  end


  #
  # PRIVATES
  #

  defp try_linestring(coords) do
    try do
      if Distance.distance(coords) == 0 do
        Geo.JSON.encode!(%Geo.Point{coordinates: List.first(coords), srid: 4326})
      else
        Geo.JSON.encode!(%Geo.LineString{coordinates: coords, srid: 4326})
      end
    rescue
      _e -> nil
    end
  end

  defp asc_nulls_last(x, y, key),    do: if is_nil(y[key]),  do: true,   else: x[key] < y[key]
  defp desc_nulls_last(x, y, key),   do: if is_nil(y[key]),  do: true,   else: x[key] > y[key]
  defp asc_nulls_first(x, y, key),   do: if is_nil(y[key]),  do: false,  else: x[key] < y[key]
  defp desc_nulls_first(x, y, key),  do: if is_nil(y[key]),  do: false,  else: x[key] > y[key]

  defp reduce_ret_field({field, als, formula, default}, acc, params, obj) do
    field = String.to_atom(field)
    als = if als,     do: String.to_atom(als), else: field
    val = if formula, do: try_execute_it(formula, Map.merge(obj, params)), else: obj[field]
    Map.put(acc, als, val || default)
  end

  defp try_evaluate(string, params) when is_list(params) do
    try do
      """
      import ShorterMaps
      #{string}
      """
      |> Code.string_to_quoted!()
      |> Helpers.in_module(__MODULE__)
      |> Code.eval_quoted(params)
      |> elem(0)
    rescue
      e -> "unexpected error: #{inspect(e)}"
    end
  end
  defp try_evaluate(string, params) when is_map(params), do: try_evaluate(string, params |> Map.to_list)

  defp try_execute_it(string, params \\ [])
  defp try_execute_it(string, params) when is_list(params) do
    try do
      """
      import ShorterMaps
      #{string}
      """
      |> Code.eval_string(params)
      |> elem(0)
    rescue
      _e -> string
    end
  end

  defp try_execute_it(string, params) when is_map(params) do
    try do
      """
      import ShorterMaps
      #{string}
      """
      |> Code.eval_string(params |> Map.to_list)
      |> elem(0)
    rescue
      _e -> string
    end
  end

  defp reduce_to_front(packs, fml), do: try_evaluate(fml, %{packs: packs})

  defp reject_errors(list) when is_list(list), do: Enum.reject(list, &rejectable_error?/1)
  defp reject_errors(map), do: map
  defp rejectable_error?(val) when is_binary(val), do: String.match?(val, ~r/error/)
  defp rejectable_error?(_), do: false

  defp get_beg(lat, lon, dev, front, db) do
    sql_request(db, make_select_deviation_request(lat, lon, dev))
    |> case do
      []    -> front
      list when is_list(list)  ->
        closest = Enum.reduce(list, %{dist: 999_999, loc: nil}, & find_closest(&1, &2, lat, lon))
        dist = closest.dist
        closest = closest.loc
        %{
          beg_lat: lat,
          beg_lon: lon,
          beg_kmpk: closest[:id],
          beg_segment: closest[:segment_id],
          beg_peregon: closest[:track_object_name],
          beg_km: closest[:point_on_track_km],
          beg_pk: round(dist / 100),
          beg_m: round(dist - div(round(dist), 100)),
        }
        _any -> %{}
    end
  end

  defp get_end(lat, lon, dev, front, db) do
    sql_request(db, make_select_deviation_request(lat, lon, dev))
      |> case do
        []    -> front
        list when is_list(list)  ->
          closest = Enum.reduce(list, %{dist: 999_999, loc: nil}, & find_closest(&1, &2, lat, lon))
          dist = closest.dist
          closest = closest.loc
          %{
            beg_lat: lat,
            beg_lon: lon,
            end_kmpk: closest[:id],
            end_segment: closest[:segment_id],
            end_peregon: closest[:track_object_name],
            end_km: closest[:point_on_track_km],
            end_pk: round(dist / 100),
            end_m: round(dist - div(round(dist), 100)),
          }
        _any -> %{}
      end
  end

  defp fetch_closest(%{beg_lat: beg_lat, beg_lon: beg_lon, end_lat: end_lat, end_lon: end_lon} = front, db, dev) do
    dev = dev or 0.3
    front
    |> Map.merge(get_beg(beg_lat, beg_lon, dev, front, db))
    |> Map.merge(get_end(end_lat, end_lon, dev, front, db))
  end

  defp find_closest(%{point_on_track_latitude: lat, point_on_track_longitude: lon} = loc, acc, beg_lat, beg_lon) do
    dist = Geocalc.distance_between([beg_lon, beg_lat], [lon |> Decimal.to_float, lat |> Decimal.to_float])

    if acc.dist < dist, do: acc, else: %{dist: dist, loc: loc}
  end

  defp make_select_deviation_request(lat, lon, dev) do
    """
    select * from spr_km
    where point_on_track_latitude  > #{lat - dev}
      and point_on_track_longitude > #{lon - dev}
      and point_on_track_latitude  < #{lat + dev}
      and point_on_track_longitude < #{lon + dev}
    """
  end
end
