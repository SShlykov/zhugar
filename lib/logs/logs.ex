defmodule Zhugar.LogIO do
  @moduledoc """
  Этот модуль помогает работать с логами
  """
  import ShorterMaps
  require Logger

  @doc """
  Эта функция достает логи из папки логс и выводит на просмотр
  """
  def read_logs("warn", date) do
    "logs/#{date}/warn.log"
    |> Path.expand(File.cwd!)
    |> File.read!
    |> String.split("\n")
    |> Enum.filter(& &1 != "")
    |> Enum.filter(& Regex.match?(~r/warn/, &1))
    |> Enum.map(& &1 |> String.split("[warn] ") |> List.last)
    |> Enum.map(& Jason.decode!(&1))
    |> Enum.with_index()
    |> Enum.map(fn {data, index} -> ~M{index, data} end)
  end

  def read_logs(log_type, date) do
    "logs/#{date}/#{log_type}.log"
    |> Path.expand(File.cwd!)
    |> File.read!
    |> String.split("\n \n")
    |> Enum.filter(& &1 != "")
    |> Enum.filter(& Regex.match?(~r/#{log_type}/, &1))
    |> Enum.map(& &1 |> String.split("[#{log_type}] ") |> List.last)
    |> Enum.with_index()
    |> Enum.map(fn {data, index} -> ~M{index, data} end)
  end

  @doc """
  Эта функция позволяет записать лог ошибок возникающих на сервере
  """
  def log_error(status, conn, error) do
    if status < 500 do
      error
      |> Map.put(:status, status)
      |> Map.put(:remote_ip, fetch_remote_ip(conn))
      |> Map.put(:body_params, fetch_body_params(conn))
      |> Jason.encode!()
      |> Logger.warn(application: :warn)
    else
      error
      |> Map.put(:status, status)
      |> Map.put(:remote_ip, fetch_remote_ip(conn))
      |> Map.put(:body_params, fetch_body_params(conn))
      |> Jason.encode!()
      |> Logger.error()
    end

    status
  end

  def fetch_remote_ip(%{remote_ip: {a, b, c, d}}), do: "#{a}.#{b}.#{c}.#{d}"
  def fetch_body_params(%{body_params: body_params}), do: body_params

end
