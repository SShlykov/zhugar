defmodule Zhugar.Requester do
  @moduledoc """
  Модуль построенный поверх стандартного драйвера для работы с PostgreSQL
  Цель написать сервис подходящий для супердинмичных запросов в базу

  obj = %{a: 1, b: "string", c: :broken}
  table_name = "test_table"
  db_auth = %{hostname: "localhost", username: "postgres", password: "postgres", database: "postgres"}
  """
  @db_auth %{hostname: "localhost", username: "postgres", password: "postgres", database: "postgres"}

  @doc """
  Инициирует процесс реализующий связь с базой данных

  ## Например

      iex> db_auth = %{hostname: "localhost", username: "postgres", password: "postgres", database: "postgres"}
      iex> {:ok, pid} = Zhugar.Requester.init_db(db_auth)
      iex> Process.alive?(pid)
      true

  """
  @spec init_db(any) :: {:error, any} | {:ok, pid}
  def init_db(db_auth \\ @db_auth), do: db_auth |> Enum.to_list |> Kernel.++([types: Zhugar.PostgresTypes]) |> Postgrex.start_link()

  @doc """
  Останавливает процесс базы данных

  ## Например

      iex> db_auth = %{hostname: "localhost", username: "postgres", password: "postgres", database: "postgres"}
      iex> {:ok, pid} = Zhugar.Requester.init_db(db_auth)
      iex> Zhugar.Requester.stop_db(pid)
      :ok

  """
  @spec stop_db(atom | pid | {atom, any} | {:via, atom, any}) :: :ok
  def stop_db(pid), do: GenServer.stop(pid)

  @doc """
  Проверяет, существует ли таблица

  ## Например

      iex> %{to_regclass: num} = Zhugar.Requester.is_table_exist?("test")
      iex> is_integer(num)
      true

  """
  @spec is_table_exist?(binary(), map()) :: any()
  def is_table_exist?(table_name, db_auth \\ @db_auth) do
    Zhugar.SqlWorker.run_query(db_auth, "SELECT to_regclass('#{table_name}');")
  end

  @doc """
  форсированная вставка - вставляет данные вместе с недостающими столбцами


  """
  @spec force_insert(binary(), map(), map()) :: :error | [any] | Postgrex.Result.t()
  def force_insert(table_name, struct, db_auth \\ @db_auth) do
    {:ok, pid} = init_db(db_auth)

    with true <- is_exist?(pid, table_name),
          columns  <- get_table_columns(pid, table_name),
          struct <- fetch_structure(struct),
          keys <- Enum.map(struct, & elem(&1, 0)),
          diff <- keys -- columns do

      if length(diff) > 0 do
        add_fields(pid, struct, diff, table_name)
        insert_data(pid, struct, table_name)
      else
        insert_data(pid, struct, table_name)
      end
    else
      _e -> :error
    end
  end

  @doc """
  Добавляет столбцы к таблице
  """
  @spec add_fields(atom | pid | {atom, any} | {:via, atom, any} | DBConnection.t(), any, any, any) :: Postgrex.Result.t()
  def add_fields(pid, struct, keys, table_name) do
    query =
      struct
      |> Enum.map(fn {key, _v, type} -> {key, type} end)
      |> Enum.reject(fn {key, _type} -> !(key in keys) end)
      |> create_add_table_column_query(table_name)

    Postgrex.query!(pid, query, [])
  end

  @doc """
  Добавляет таблицу в базе по описанной структуре
  """
  @spec create_if_not_exist(any, map, map()) :: :noop | Postgrex.Result.t()
  def create_if_not_exist(table_name, struct, db_auth \\ @db_auth) do
    {:ok, pid} = init_db(db_auth)
    struct = fetch_structure(struct)

    res =
      if is_exist?(pid, table_name) do
        :noop
      else
        try do
          create_table(pid, struct, table_name)
          insert_data(pid, struct, table_name)
        rescue
          _e ->
            remove_table(pid, table_name)
            :noop
        catch
          _e -> :noop
        end
      end
    stop_db(pid)
    res
  end

  @spec get_table_columns(atom | pid | {atom, any} | {:via, atom, any} | DBConnection.t(), any) :: [any]
  def get_table_columns(pid, table_name) do
    pid
    |> Postgrex.query!("select * from #{table_name}", [])
    |> Map.get(:columns)
    |> Kernel.--(["id"])
  end

  def is_exist_mannually?(table_name, db_auth \\ @db_auth) do
    {:ok, pid} = init_db(db_auth)
    res = Postgrex.query!(pid, "SELECT to_regclass('#{table_name}');", [])
    stop_db(pid)
    res
  end

  @spec is_exist?(atom | pid | {atom, any} | {:via, atom, any} | DBConnection.t(), binary) :: boolean
  def is_exist?(pid, table_name) when is_binary(table_name), do: Postgrex.query!(pid, "SELECT to_regclass('#{table_name}');", []) |> Map.get(:rows) |> List.first() |> List.first() |> is_nil() |> Kernel.!()
  def create_table(pid, struct, table_name), do: Postgrex.query!(pid, struct |> create_table_builder(table_name), [])
  def remove_table(pid, table_name), do: Postgrex.query!(pid, "DROP TABLE #{table_name};", [])

  def insert_data(pid, struct, table_name) do
    list = struct |> Enum.map(fn {_x, val, _type} -> val end)
    keys = struct |> Enum.map(fn {key, _v, _type} -> key end)
    query = create_insert_query(length(list), keys, table_name)
    Postgrex.query!(pid, query, list)
  end

  @spec fetch_structure(map) :: [any]
  def fetch_structure(struct) when is_struct(struct), do: Zhugar.Utils.to_storeable_map(struct) |> fetch_structure()

  def fetch_structure(struct) when is_map(struct) do
    struct
    |> Enum.to_list()
    |> Enum.map(fn {key, value} -> {Atom.to_string(key), value, what_type(value)} end)
    |> Enum.reject(& is_nil(elem(&1, 2)))
  end

  @doc """
  Создает запрос на вставку колонок в таблицу

  ## Например

      iex> Zhugar.Requester.create_add_table_column_query([{"a", "integer"}, {"b", "varchar(255)"}], "test")
      "ALTER TABLE test  ADD COLUMN a integer, ADD COLUMN b varchar(255);"

  """
  @spec create_add_table_column_query(any, any) :: binary()
  def create_add_table_column_query(params, table_name) do
    cols =
      params
      |> Enum.reduce("", & "#{&2} ADD COLUMN #{elem(&1, 0)} #{elem(&1, 1)},")
      |> String.slice(0..-2)
      |> Kernel.<>(";")

    "ALTER TABLE #{table_name} #{cols}"
  end


  @doc """
  Создает запрос на вставку данных в таблицу

  ## Например

      iex> Zhugar.Requester.create_insert_query(2, ["a", "b"], "test")
      "insert into test ( a, b) values ( $1, $2)"

      iex> Zhugar.Requester.create_insert_query(3, ["a", "b", "c"], "test")
      "insert into test ( a, b, c) values ( $1, $2, $3)"

  """
  @spec create_insert_query(integer, any, any) :: binary()
  def create_insert_query(len, keys, table_name) do
    vals =
      1..len
      |> Enum.reduce("(", & "#{&2} $#{&1},")
      |> String.slice(0..-2)
      |> Kernel.<>(")")

    keys =
      keys
      |> Enum.reduce("(", & "#{&2} #{&1},")
      |> String.slice(0..-2)
      |> Kernel.<>(")")

    "insert into #{table_name} #{keys} values #{vals}"
  end


  @doc """
  Создает запрос, который создает таблицу с заданным названием и структурой

  ## Например

      iex> Zhugar.Requester.create_table_builder([{"a", "mystring", "varchar(250)"}], "test")
      "CREATE TABLE test (id bigserial primary key, a varchar(250) default null);"

      iex> Zhugar.Requester.create_table_builder([{"a", "mystring", "varchar(250)"}, {"b", 250, "integer"}], "test")
      "CREATE TABLE test (id bigserial primary key, a varchar(250) default null, b integer default null);"

  """
  @spec create_table_builder(maybe_improper_list, any) :: binary()
  def create_table_builder(param_list, table_name) when is_list(param_list) do\
    param_list
    |> Enum.reduce("CREATE TABLE #{table_name} (id bigserial primary key,", fn {key, _value, type}, acc ->
      acc <> " #{key} #{type} default null,"
    end)
    |> String.slice(0..-2)
    |> Kernel.<>(");")
  end

  @doc """
  Определяет тип данных, которые будут использованы при создании таблицы в postgres

  ## Например

      iex> Zhugar.Requester.what_type("I AM NAME")
      "varchar(255)"

      iex> Zhugar.Requester.what_type(3)
      "integer"

      iex> Zhugar.Requester.what_type(35000000000)
      "bigint"

      iex> Zhugar.Requester.what_type(%{a: 2})
      "json"

      iex> Zhugar.Requester.what_type(true)
      "boolean"

  """
  @spec what_type(any()) :: nil | binary()
  def what_type(%NaiveDateTime{} = _value), do: "timestamp"
  def what_type(%Date{} = _value), do: "date"
  def what_type(value) when is_list(value), do: "integer"
  def what_type(value) when is_boolean(value), do: "boolean"
  def what_type(value) when is_float(value), do: "decimal"
  def what_type(value) when is_integer(value) do
    if abs(value) > 100_000_000, do: "bigint", else: "integer"
  end
  def what_type(value) when is_binary(value), do: "varchar(255)"
  def what_type(value) when is_map(value), do: "json"
  def what_type(_), do: nil
  def nne do
    :nne
  end
end
