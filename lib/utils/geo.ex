defmodule Zhugar.Geo do
  @moduledoc false

  def decode_geo(geo) when is_binary(geo), do: Geo.WKB.decode!(geo)

  def decode_geo(data, key) when is_list(data), do:  Enum.map(data, & decode_geo(&1, key))
  def decode_geo(data, key) do
    geo = data[key]
    if not is_nil(geo) do
      case Geo.WKB.decode(geo) do
        {:ok, g} -> data |> Map.put(key, g)
        _        -> data |> Map.delete(key)
      end
    else
      data
    end
  end
end
