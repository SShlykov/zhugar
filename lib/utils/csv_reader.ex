defmodule Zhugar.CSVReader do
  @moduledoc """
  Получет список объектов из csv файлов
  """
  def get(path) do
    path
    |> File.stream!
    |> CSV.decode(headers: true)
    |> Stream.map(fn {:ok, map} ->
      map |> Morphix.atomorphiform!
    end)
    |> Stream.map(& nilify_emptyness(&1))
    |> Enum.to_list

  end

  def nilify_emptyness(t) do
    Map.keys(t)
    |> Enum.reduce(%{}, fn key, obj ->
      cond do
        t[key] == "" or  is_nil(t[key]) -> obj
        Regex.scan(~r/_id/, to_string(key)) != [] -> Map.put(obj, key, to_int(t[key]))
        t[key] -> Map.put(obj, key, validate_data(t[key]))
      end
    end)
  end

  def validate_data(data) when is_binary(data) do
    cond do
      Regex.scan(~r/{/, data) != [] and Regex.scan(~r/}/, data) != [] -> str_to_list(data)
      true -> data
    end
  end

  def validate_data(data), do: data

  def to_int(str) do
    try do
      String.to_integer(str)
    rescue
      _e -> str
    end
  end

  def str_to_list(str) do
    str
    |> String.split("{")
    |> List.last()
    |> String.split("}")
    |> List.first()
    |> String.split(",")
    |> Enum.map(& String.trim(&1))
  end
end
