defmodule Zhugar.SqlWorker do
  @moduledoc """
  Модуль для работы с sql запросами
  """
  require Logger

  def make_server(:postgres, url) do
    try do
      Zhugar.SqlWorker.Repo.start_link([url: url])
    rescue
      _e -> {:error, "Unable to start repo with current params"}
    end
  end

  def run_query(%{hostname: _h_name, username: _u_name, password: _pwd, database: _db_name} = db_params, query, params \\ []) do
    try do
      %{columns: columns, rows: rows} = run_query(:no_normalization, db_params, query, params)
      cols =  Enum.map(columns, &String.to_atom/1)

      rows
      |> Enum.map(fn row -> Enum.zip(cols, row) end)
      |> case do
        list when is_list(list) and length(list) == 1 -> List.first(list) |> Enum.reduce(%{}, fn {k, v}, acc -> Map.put(acc, k, v) end)
        list when is_list(list) -> Enum.map(list, & Enum.reduce(&1, %{}, fn {k, v}, acc -> Map.put(acc, k, v) end))
      end
    rescue
      e ->
        Logger.warn(inspect(e))
        {:error, "Unable to start repo with current params"}
    end
  end

  def run_query(:no_normalization, %{hostname: _h_name, username: _u_name, password: _pwd, database: _db_name} = db_params, query, params) do
    {:ok, pid} = db_params |> Enum.to_list |> Kernel.++([types: Zhugar.PostgresTypes]) |> Postgrex.start_link()
    res = Postgrex.query!(pid, query, params || [])
    GenServer.stop(pid)
    res
  end
end
