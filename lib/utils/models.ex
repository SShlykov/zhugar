defmodule Zhugar.Model do
  @moduledoc """
  Вспомогательные тулы для моделей
  """

  def g(val), do: Map.get(val, :id)
  def g(val, atom) when is_atom(atom), do: Map.get(val, atom)
end
