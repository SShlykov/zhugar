import Config

config :zhugar, author: "sshlykov"
config :zhugar, Zhugar.SqlWorker.Repo, adapter: Ecto.Adapters.Postgres
config :tesla, adapter: Tesla.Adapter.Hackney
config :geo_postgis, json_library: Jason

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

import_config "#{Mix.env}.exs"
