defmodule Zhugar.ControllerFuncs do
  @moduledoc """
  Этот помошник содержит нектороые функции помогающие в реализации контроллера
  и отлова ошибок
  """
  alias Zhugar.LogIO

  def fetch_status(message) do
    cond do
      Regex.match?(~r/not supported/, message) -> 400
      Regex.match?(~r/Unauthorized/, message) -> 401
      Regex.match?(~r/Unable/, message) -> 406
      Regex.match?(~r/has no/, message) -> 406
      Regex.match?(~r/allready has/, message) -> 406
      true -> 500
    end
  end

  def fetch_status_and_log(conn, error, message) do
    message
    |> fetch_status()
    |> LogIO.log_error(conn, error)
  end
end
