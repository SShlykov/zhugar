defmodule Zhugar.DT do
  @moduledoc """
  Помошники для работы с временем
  """
  import ShorterMaps

  @type time_diaposon :: %{start: binary(), finish: binary()}
  @type diaps :: binary()
  @type error :: {:error, binary()}

  @doc """
  Берет даты из приходящих параметров и возвращает мапу типа
  Проверяет, что дата start <= finish

  ## Например

      iex> Zhugar.DT.get_date_or_create(%{"start" => "2020-09-13", "finish" => "2020-09-15"})
      %{finish: ~N[2020-09-15 00:00:00], start: ~N[2020-09-13 00:00:00]}

      iex> Zhugar.DT.get_date_or_create(%{"start" => "2020-09-13", "finish" => "2020-09-10"})
      {:error, "Start can`t be less then finish."}

      iex> Zhugar.DT.get_date_or_create(%{"start" => "2020-09-13"})
      %{finish: nil, start: ~N[2020-09-13 00:00:00]}

      iex> %{finish: nil, start: %NaiveDateTime{} = any_time} = Zhugar.DT.get_date_or_create(%{})
      iex> is_nil(any_time)
      false

  """
  @spec get_date_or_create(map() | time_diaposon) :: time_diaposon | error()
  def get_date_or_create(params) do
    start = get_date_or_create(params, "start")
    finish = get_date_or_create(params, "finish")
    case start <= finish do
      true  -> ~M{start, finish}
      false when is_nil(finish) -> ~M{start, finish}
      false -> {:error, "Start can`t be less then finish."}
    end
  end

  @spec get_date_or_create(map, diaps()) :: nil | {:error, any} | %NaiveDateTime{}
  def get_date_or_create(params, "start") do
    case Map.get(params, "start")  do
      nil   -> Timex.now |> Timex.to_naive_datetime()
      data  -> parse_time(data)
    end
  end

  def get_date_or_create(params, "finish") do
    case Map.get(params, "finish")  do
      nil   -> nil
      data  -> parse_time(data)
    end
  end

  @doc """
  Парсит дату или datetime
  """

  @spec parse_time(binary()) :: %NaiveDateTime{}
  def parse_time(time_string) when is_binary(time_string) do
    case Timex.parse(time_string, "{ISO:Extended}") do
      {:error, _} -> Timex.parse!(time_string, "%Y-%m-%d", :strftime)
      {:ok, time} -> time
    end
  end
end
