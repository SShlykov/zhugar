defmodule Zhugar.MixProject do
  use Mix.Project

  def project do
    [
      app: :zhugar,
      description: "This fly into the sky",
      version: "0.1.0-dev",
      elixir: "~> 1.10",
      compilers: [:gettext] ++ Mix.compilers(),
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package(),
      name: "Zhugar",
      docs: [main: "Zhugar"]
    ]
  end

  defp package do
    [
      maintainers: [" sshlykov "],
      licenses: ["MIT"],
      links: %{"GitHub" => "https://github.com/sshlykov"}
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 1.5", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
      {:benchee, "~> 1.0", only: :dev},
      {:ex_doc, "~> 0.23.0", only: [:dev, :test], runtime: false},
      {:ecto_sql, "~> 3.0"},
      {:postgrex, ">= 0.0.0"},
      {:morphix, "~> 0.8.0"},
      {:poison, "~> 4.0"},
      {:csv, "~> 2.4"},
      {:tesla, "~> 1.4"},
      {:shorter_maps, "~> 2.0"},
      {:gettext, ">= 0.0.0"},
      {:geo, "~> 3.0"},
      {:geo_postgis, "~> 3.1"},
      {:topo, "~> 0.4.0"},
      {:timex, "~> 3.5"},
      {:recase, "~> 0.5"},
      {:geocalc, "~> 0.5"},
      {:distance, "~> 0.2.1"},
    ]
  end
end
