defmodule Zhugar do
  @moduledoc """
  Documentation for `Zhugar`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Zhugar.hello()
      :world

  """
  def hello do
    :world
  end
end
