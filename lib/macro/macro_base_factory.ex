defmodule Zhugar.Makro do
  @moduledoc """
  Макросы для создания фабрик
  """

  @doc """
  Макрос, позволяющий создать стандартную функцию множественной вставки данных в таблицу
  принимает в себя название контекста и название сущности

    ## Например

    base_multisert(:devices, :machines)

    Вернет завернутый в try - catch вызов Database.Devices.multisert_machines(data)

    ввиде исполняемой в дальнейшем функции multisert(data)
  """
  defmacro base_multisert(context, entity) do
    quote do
      import Zhugar.Modules, only: [name_to_module_name: 1, fetch_main_module_name: 0]
      def unquote(:multisert)(data) do
        try do
          "Elixir.#{fetch_main_module_name()}.#{name_to_module_name(unquote(context))}"
          |> String.to_existing_atom()
          |> apply(unquote(:"multisert_#{entity}s"), [data])

        rescue
          e -> {:error, e}
        end
      end
    end
  end

  defmacro multisert(context, entity, ce \\ 300) do
    quote do
      import Zhugar.Modules, only: [name_to_module_name: 1, fetch_main_module_name: 0]
      alias Database.Repo
      alias Zhugar.DBR
      require Logger
      def unquote(:"multisert_#{to_string(entity)}s")(data) do
        strt =
          "Elixir.#{fetch_main_module_name()}.#{name_to_module_name(unquote(context))}.Models.#{name_to_module_name(unquote(entity))}"
          |> String.to_existing_atom()

        result =
          data
          |> Stream.map(fn ent -> strt.__struct__ |> strt.admin_changeset(ent) end)
          |> Enum.split_with(& &1.valid?)
          |> case do
             {valid, _invalid} ->
                valid
                |> Stream.map(& Ecto.Changeset.apply_changes(&1))
                |> Stream.map(&
                  DBR.to_storeable_map(&1) |> Zhugar.CSVReader.nilify_emptyness()
                )
              e ->
                Logger.warn(inspect(e))
                throw("data is invalid")
            end
          |> Stream.chunk_every(unquote(ce), unquote(ce))
          |> Stream.with_index()
          |> Enum.reduce(Ecto.Multi.new, fn ({data, index}, acc) ->  Ecto.Multi.insert_all(acc, :"insert_all#{index}", strt, data) end)
          |> Repo.transaction()

        case result do
          {:ok, entity} -> entity
          {:error, _, changeset, _} ->  {:error, changeset}
        end
      end
    end
  end

  defmacro geo_multisert(context, entity, geo_field) do
    quote do
      import Zhugar.Modules, only: [name_to_module_name: 1, fetch_main_module_name: 0]
      import Zhugar.Geo, only: [decode_geo: 2]
      def unquote(:multisert)(data) do
        try do
          decoded =
            data
            |> decode_geo(unquote(geo_field))

          "Elixir.#{fetch_main_module_name()}.#{name_to_module_name(unquote(context))}"
          |> String.to_existing_atom()
          |> apply(unquote(:"multisert_#{entity}s"), [decoded])

        rescue
          e -> {:error, e}
        end
      end
    end
  end
end
