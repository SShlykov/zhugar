defmodule Zhugar.HttpWorker do
  @moduledoc """
  Модуль позволяющий делать кастомные запросы к HTTP API
  """
  @type resp_or_error :: {:error, binary()}
                      | map()
                      | [map()]

  @type body :: binary()
              | maybe_improper_list(
                binary() | maybe_improper_list(any(), binary() | []) | byte(),
                binary() | []
              )

  @type query :: list({atom(), any()})


  @doc """
  Упрощенная форма запроса к API принимает мапу с параметрами запроса и боди в собранном в JSON виде либо в виде строки

  ## Например

      iex> Zhugar.HttpWorker.run_http(%{addr: "https://jsonplaceholder.typicode.com/posts", method: "post"}, ~s({"id": 101, "title": "foo", "body": "bar", "userId": 1}))
      %{body: "bar", id: 101, title: "foo", userId: 1}

  """
  def run_http(%{addr: addr, method: method} = pms, body \\ nil) do
    headers = encode_headers(pms, body)
    query   = Map.get(pms, :query) || []
    body    = encode_body(body)

    try do
      http_request(method, addr, body, headers, query)
    rescue
      e -> {:error, "Query Error: #{e}"}
    end
  end

  @doc """
  Делает запрос к указанному API серверу и принимает все необходимые параметры

  ## Например

      iex> Zhugar.HttpWorker.http_request("post", "https://jsonplaceholder.typicode.com/posts", "{\\\"id\\\": 101, \\\"title\\\": \\\"foo\\\", \\\"body\\\": \\\"bar\\\", \\\"userId\\\": 1}", [{"content-type", "application/json"}], [])
      %{body: "bar", id: 101, title: "foo", userId: 1}
  """
  @spec http_request(binary(), binary(), body(), maybe_improper_list(), query()) :: resp_or_error
  def http_request(method, addr, body, headers, query) do
    case method do
      "get"     -> Tesla.get(addr, headers: headers, query: query)
      "post"    -> Tesla.post(addr, body, headers: headers)
      "put"     -> Tesla.put(addr, body, headers: headers, query: query)
      "patch"   -> Tesla.patch(addr, body, headers: headers, query: query)
      "delete"  -> Tesla.delete(addr)
      _         -> {:error, "unresolved method"}
    end
    |> fetch_response
    |> case do
      list when is_list(list) -> Enum.map(list, &Morphix.atomorphify!/1)
      map when is_map(map)    -> Morphix.atomorphify!(map)
      {:error, _} = err       -> err
    end
  end

  defp encode_headers(params, nil) do
    case Map.get(params, :headers) do
      nil -> []
      list when is_list(list) -> list
      _ ->  throw("headers could not be read")
    end
  end

  defp encode_headers(params, _body) do
    case Map.get(params, :headers) do
      nil                     -> [{"content-type", "application/json"}]
      list when is_list(list) -> [{"content-type", "application/json"}] ++ list
      _                       -> throw("could not be read")
    end
  end

  defp encode_body(body) when is_binary(body), do: body |> Poison.decode!() |> Poison.encode!()
  defp encode_body(body) when is_map(body), do: Poison.encode!(body)
  defp encode_body(nil), do: nil
  defp encode_body(_), do: nil

  defp fetch_response(response) do
    case response do
      {:ok, %{status: status, body: body}} when status < 300 and status >= 200 -> Poison.decode!(body)
      {:ok, %{status: status, body: body}} -> {:error, %{status: status, body: Poison.decode!(body)}}
      {:error, :nxdomain} -> {:error, "addr wont be succeded"}
      _e                  -> {:error, "Response can`t be parsed"}
    end
  end
end
