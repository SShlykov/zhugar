defmodule Zhugar.SqlWorker.Repo do
  use Ecto.Repo,
    otp_app: :zugar,
    adapter: Ecto.Adapters.Postgres
end
