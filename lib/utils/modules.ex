defmodule Zhugar.Modules do
  @moduledoc """
  Этот хэлпер нужен для более удобной работы с модулями
  """

  def make_module_name(ent, main_module) when is_atom(main_module) do
    "Elixir.#{name_to_module_name(main_module)}.Locations.#{name_to_module_name(ent)}"
    |> String.to_existing_atom
  end

  def fetch_main_module_name do
    Mix.Project.config[:app]
    |> name_to_module_name()
  end

  def name_to_module_name(name) when is_atom(name) do
    name
    |> to_string()
    |> String.split("_")
    |> Enum.map(& String.capitalize(&1))
    |> Enum.join
  end

  def name_to_module_name(name) when is_binary(name) do
    name
    |> String.split("_")
    |> Enum.map(& String.capitalize(&1))
    |> Enum.join
  end
end
