defmodule Zhugar.Calculator.Helpers do
  @moduledoc """
  Zhugar macroses for Calculator
  """
  def in_module(ast, mod) do
    quote do
      import unquote(mod)
      import Kernel
      unquote(ast)
    end
  end

  def locals_calls_only(ast) do
    ast
    |> Macro.prewalk(fn
      evil = {{:., _, _}, _, _} ->
        IO.puts("warning: removed non local call #{inspect(evil)}")
        nil

      {:eval, _, args} when is_list(args) ->
        IO.puts("warning: removed call to eval")
        nil

      code -> code
    end)
  end
end
